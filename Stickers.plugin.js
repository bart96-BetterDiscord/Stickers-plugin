//META {"name":"Stickers", "website":"https://github.com/bart96-b/discord-stickers","source":"https://raw.githubusercontent.com/bart96-b/discord-stickers/master/Stickers.plugin.js"}*//

class Stickers {
	getName				() {return 'Stickers';}
	getDescription() {return 'Add, send and delete personal stikers.';}
	getVersion		() {return '1.1.0';}
	getAuthor			() {return 'BART96';}

	constructor() {
		this.fs = require("fs");
		this.path = require('path');
		this.process = require("process");

		this.filesDir = this.path.resolve(this.pluginsFolder.join('/'), this.getName() +'Files');
		this.stickers = {};
	}


	unload        () {}
	stop          () {}

	load          () {}
	start         () {
		this.uploads = BDV2.WebpackModules.findByUniqueProperties(["instantBatchUpload"]);
		this.selectedChannel = BDV2.WebpackModules.findByUniqueProperties(['getLastSelectedChannelId']);
		this.channels = BDV2.WebpackModules.findByUniqueProperties(['getChannels']);

		this.addLibs();
		this.getAllStickers();
		this.setup();

		$('body').on('click', '.guild-inner.da-guildInner,[class^="containerDefault"],[href^="/channels/@me/"]', () => setTimeout(() => this.setup(), 0));
	}


	addLibs() {
		let head = $('head').first();

		this.fs.readFile(this.path.join(this.filesDir, 'style.css'), 'utf8', (err, data) => {
			if (err) return console.log(err);

			head.append($('<style>', {id:this.getName()+'Style', text:data}))
		});
	}


	getAllStickers() {
		this.fs.readdir(this.filesDir, (err, folders) => {
			if (err) return console.log(err);

			folders.filter(folder => !/\./.test(folder)).forEach(folder => {
				this.fs.readdir(this.path.join(this.filesDir, folder), (err, files) => {
					if (err) return console.log(err);

					files.filter(file => /\.(?:png|jpg|jpeg|gif)$/.test(file)).forEach(file => {
						this.fs.readFile(this.path.join(this.filesDir, folder, file), (err, data) => {
							if (err) return console.log(err);

							if (this.stickers[folder] == undefined) this.stickers[folder] = {};

							this.stickers[folder][file] = {
								base64: Buffer.from(data).toString('base64'),
								file: new File([data], file, {type:'image/'+file.split('.').pop}),
							};
						});
					});
				});
			});
		});
	}

	setup() {
		let form = $('[class^="channelTextArea"]');

		form.append($('<div>', {
			id: this.getName() +'Btn',
			html: '<div>&lt;</div>',
			click: () => this.show(form),
			style: 'display:absolute;',
		}));
	}


	_eventSearch(event) {
		let inputText = event.target.value;
	}


	show(form) {
		let emojiBtn = form.find('[class^="emojiButton"]').click();

		let search = $('<input>', {
			type: 'text',
			placeholder: 'Search...',
			// input: event => this._eventSearch(event),
			// propertychange: event => this._eventSearch(event),
		});
		let update = $('<span>', {
			html: '&#8635;',
			click: () => {},
		});
		let download = $('<span>', {
			html: '<svg width="20" height="20" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg" ratio="1"><path fill="none" stroke="#000" stroke-width="1.1" d="M6.5,14.61 L3.75,14.61 C1.96,14.61 0.5,13.17 0.5,11.39 C0.5,9.76 1.72,8.41 3.3,8.2 C3.38,5.31 5.75,3 8.68,3 C11.19,3 13.31,4.71 13.89,7.02 C14.39,6.8 14.93,6.68 15.5,6.68 C17.71,6.68 19.5,8.45 19.5,10.64 C19.5,12.83 17.71,14.6 15.5,14.6 L12.5,14.6"></path> <polyline fill="none" stroke="#000" points="11.75 16 9.5 18.25 7.25 16"></polyline> <path fill="none" stroke="#000" d="M9.5,18 L9.5,9.5"></path></svg>',
			click: () => {},
		});


		let parentEmoji = $('[class^="emojiPicker"]').parent().append($('<div>', {id:this.getName()+'Content'}));
		let content = parentEmoji.children('#'+ this.getName() +'Content').append($('<div>', {
			id:this.getName() +'Settings',
			html: search.add(update).add(download),
		}));

		$('[class^="emojiPicker"]').remove();

		for(let folder in this.stickers) {
			let group = folder.split('_');

			content.append($('<div>', {
				class: this.getName() +'GroupName',
				text: `${group[1]} (${group[0]})`,
			}));

			for(let sticker in this.stickers[folder]) {
				content.append($('<div>').append($('<img>', {
					src: 'data:image/png;base64,'+ this.stickers[folder][sticker].base64,
					name: sticker,
					click: () => {
						this.send(folder, sticker);
						emojiBtn.click();
					},
				})));
			}

		}
	}


	send(folder, sticker) {
		this.uploads.instantBatchUpload(this.selectedChannel.getChannelId(), [this.stickers[folder][sticker].file]);
	}


	get pluginsFolder()  {
		switch (this.process.platform) {
			case "win32": return [process.env.appdata, 'BetterDiscord/plugins'];
			case "darwin": return [process.env.HOME, 'Library/Preferences', 'BetterDiscord/plugins'];

			default: return [process.env.HOME, '.config', 'BetterDiscord/plugins'];
		}
	}

}
