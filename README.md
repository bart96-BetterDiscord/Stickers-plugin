# Discord – Stickers
Add, send and delete personal stikers

![img](https://puu.sh/ASTDo/25da29ab3d.png)

## Installation (Windows)
* Install official [BetterDiscord](https://betterdiscord.net) (or alternative [BetterDiscordApp](https://github.com/rauenzi/BetterDiscordApp), if there are any errors)
* [Download](https://github.com/bart96-b/discord-stickers/archive/master.zip) archive with plugin files
* Open `Discord` -> `User settings` -> `Plugins`
* Click on button `Open plugins folder`
* Copy files from archive to this folder
* Restart Discord
* Open `Discord` -> `User settings` -> `Plugins`
* Enable plugin `Stickers`

## Conditions
Image dimensions should be 128x128 pixels!
